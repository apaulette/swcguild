function validateForm() {
	 var fieldName = document.forms["owForm"]["name"].value;
	 var fieldEmail = document.forms["owForm"]["emailAddress"].value;
	 var fieldPhone = document.forms["owForm"]["phone"].value;
	 var fieldReason = document.forms["owForm"]["reason"].value;
	 var fieldAddInfo = document.forms["owForm"]["addInfo"].value;

	if (fieldName == null || fieldName == "") {
		alert("Name must be filled out.");
		return false;
	}

	if ((fieldEmail == null || fieldEmail == "") && (fieldPhone == null || fieldPhone == "")) {
		alert("At least one method of contact is required.\n\nPlease provide either an email address or a phone number.");
		return false;
	}

	if ((fieldReason == "OTHER") && (fieldAddInfo == null || fieldAddInfo == "")) {
		alert("In order to serve you better, Additional Information\nis a required field for this type of inquiry.\n\nPlease enter your questions or concerns\nin the form text box.");
		return false;
	}

	var dayArray = document.getElementsByName('contactDays[]');
	var dayChecked = false;
	for (i=0; i < dayArray.length; i++) {
		if (dayArray[i].checked) {
			dayChecked = true;
			break;
		}
	}
	if (dayChecked == false) {
		alert("Please select at least one day of the week\nthat is best to contact you.");
		return false;
   	}
	return true;
}