function luckySevens() {
	wager = document.getElementById("wager").value;
	var wagerValue = wager.replace("$", "");
	var money = parseInt(wagerValue);

	if (money == 0) {
		window.alert("Please wager at least *some* money!\n\nNo one plays for free here, friend.");
	} else if (money > 100) {
		window.alert("Please wager no more than $100.00.\n\nFor help with complusive gambling,\nplease consider visiting gamblersanonymous.org.");
	} else if (money <= 100) {
	var wagerNumber = [];

	while (money > 0) {
		var dieNums = [1,2,3,4,5,6];
		var dieOne = dieNums[Math.floor(Math.random() * dieNums.length)];
		var dieTwo = dieNums[Math.floor(Math.random() * dieNums.length)];
		var rollTotal = dieOne + dieTwo;
	
			if (rollTotal == 7) {
				rollWinnings = 4;
				money = money + rollWinnings;
			} else if (rollTotal != 7) {
				rollWinnings = -1;
				money = money + rollWinnings;
			}
		wagerNumber.push(money);
		}

// Generate Results and Write Table
	var endRollsN = wagerNumber.length;
	var moneyHighN = Math.max.apply(Math, wagerNumber);
	var moneyHighCountN = (wagerNumber.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0)) + 1;

	document.getElementById("startingBet").innerHTML = ('$' + wagerValue);
	document.getElementById("endRolls").innerHTML = (endRollsN);
	document.getElementById("moneyHigh").innerHTML = ('$' + moneyHighN);
	document.getElementById("moneyHighCount").innerHTML = (moneyHighCountN);

// Toggle Display of Play Buttons and Results Table
	document.getElementById("playFirst").style.display = "none";
	document.getElementById("playAgain").style.display = "block";
	document.getElementById("results").style.display = "block";
	}
}


// Toggle Display of Play Buttons and Results Table
	function luckySevensReplay() {
		document.getElementById("playFirst").style.display = "block";
		document.getElementById("playAgain").style.display = "none";
		document.getElementById("results").style.display = "none"
		document.getElementById("betForm").reset();
	}